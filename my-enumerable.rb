module Enumerable
    def my_each 
        index = 0
        while index < self.count
            yield(self[index])
            index += 1
        end
    end

    def my_each_with_index 
        index = 0
        while index < self.count
            yield(self[index],index)
            index += 1
        end
    end

    def my_select
        my_list = []
        self.my_each{|element| my_list << element if yield(element)}
        my_list
    end

    def my_all?
        result = true
        self.my_each{|element| result = false unless yield(element)}
        result
    end

    def my_any?
        result = false
        self.my_each{|element| result = true if yield(element)}
        result
    end

    def my_none?
        result = true
        self.my_each{|element| result = false if yield(element)}
        result
    end

    def my_count
        total = 0
        self.my_each{total += 1}
        total
    end

    def my_map(my_proc=nil)
        result = []
        unless my_proc == nil
            self.my_each{|element| result << (my_proc.call element)}  
        else
            self.my_each{|element| result << yield(element)}
        end
        result
    end

    def my_inject start_value
        self.my_each{|element| start_value = yield(start_value,element)}
        start_value
    end

end



=begin
puts ["h","i","hi","hello"].each{|element| puts element}
puts ["h","i","hi","hello"].my_each{|element| puts element}

puts ["h","i","hi","hello"].each_with_index{|element,index| puts element + index.to_s}
puts ["h","i","hi","hello"].my_each_with_index{|element,index| puts element + index.to_s}

puts ["h","i","hi","hello"].select{|element| element[0] == "h"}
puts ["h","i","hi","hello"].my_select{|element| element[0] == "h"}

puts ["h","i","hi","hello"].all?{|element| element.is_a? String}
puts ["h","i","hi","hello"].my_all?{|element| element.is_a? String}

puts [3,3,3,"hello"].any?{|element| element.is_a? String}
puts [3,3,3,3].my_any?{|element| element.is_a? String}

puts [3,3,3,"hello"].none?{|element| element.is_a? String}
puts [3,3,3,3].my_none?{|element| element.is_a? String}

puts [3,3,3,3].count
puts [3,3,3,3].my_count

puts ["h","i","hi","hello"].map{|element| element.upcase}
puts ["h","i","hi","hello"].my_map{|element| element.upcase}

puts [1, 2, 3, 4].inject(0) { |result, element| result + element }
puts [1, 2, 3, 4].my_inject(0) { |result, element| result + element }

def multiply_els array
    array.my_inject(1){|total, element| total * element}
end

puts multiply_els([2,4,5])

my_proc = Proc.new do |element| 
    element.upcase
end
puts ["h","i","hi","hello"].map(&my_proc)
puts ["h","i","hi","hello"].my_map my_proc
puts ["h","i","hi","hello"].my_map{|element| element.capitalize}
puts ["h","i","hi","hello"].my_map(my_proc){|element| element.capitalize}
=end