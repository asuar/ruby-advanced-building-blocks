def bubble_sort array
    swap_tracker = true
    while swap_tracker
        swap_tracker = false
        array.each_with_index.map do |element, index|
            if index < (array.count-1) && array[index] > array[index+1]
                    array[index],array[index+1] = array[index+1],array[index]
                    swap_tracker = true
            end
        end
    end
    array
end

puts bubble_sort([4,3,78,2,0,2]).join(", ")

def bubble_sort_by array
    swap_tracker = true
    while swap_tracker
        swap_tracker = false
        array.each_with_index.map do |element, index|
            if index < (array.count-1)
                compare =  yield( array[index],array[index+1])
                 if compare == 1
                    array[index],array[index+1] = array[index+1],array[index]
                    swap_tracker = true
                end
            end
        end
    end
    array
end


result = bubble_sort_by(["hi","hello","hey"]) do |left,right|
  left.length - right.length
 end

 puts result.join(", ")