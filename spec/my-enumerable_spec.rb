require './my-enumerable.rb'

describe Enumerable do

    describe "#my_select" do
      it 'works with numbers' do
        expect([1,2,32,'4'].my_select{|element| element == 2}).to eql([2])
      end

      it 'works with chars' do
        expect(['a','4','ab'].my_select{|element| element == 'a'}).to eql(['a'])
      end
    end

end